# Experimental Primitive PostgreSQL Replication

## Wait, what?

eppr is just a little, simple experiment for creating a simple trigger based asynchronous multi-master merge replication for PostgreSQL 8.4+ using only standard PostgreSQL stuffs, like pl/pgsql, dblink and pgAgent. It's only for Proof-of-Concepct reasons. Seriously, **not for real-world use**!