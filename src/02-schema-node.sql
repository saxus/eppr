﻿create extension if not exists dblink;

drop schema if exists __eppr cascade;


create schema __eppr;

create table __eppr.tables
(
   table_schema varchar(64) not null,
   table_name varchar(64) not null,
   
   primary key (table_schema, table_name)
);


create table __eppr.settings
(
  masterhost varchar(400) not null  
);