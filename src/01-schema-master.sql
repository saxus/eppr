﻿drop schema if exists __eppr_master;

create schema __eppr_master;

create table __eppr_master.tables
(
   table_schema varchar(64) not null,
   table_name varchar(64) not null,
   
   primary key (table_schema, table_name)
);